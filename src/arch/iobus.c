#include "iobus.h"

void outb(u16 addr, u8 val)
{
    __asm__("outb %%al, %%dx\n" \
            :: "d"(addr), "a"(val));
}

void outl(u16 addr, u32 val)
{
    __asm__("outl %%eax, %%dx\n" \
            :: "d"(addr), "a"(val));
}

int inl(u16 addr)
{
    int val;
    __asm__ ("inl %%dx, %%eax\n"
             : "=a"(val)
             : "d" (addr)
             );
    return val;
}

u8 inb(u16 port)
{
    u8 val;
    __asm__ ("inb %%dx, %%al\n"
            : "=a"(val)
            : "d"(port));
    return val;
}
