#include "types.h"
#include "utils.h"
#include "print.h"
#include <arch/isr.h>
#include <arch/iobus.h>

struct idt_entry
{
    u16 low_addr;
    u16 segment_selector;
    u8 im_zero;
    u8 flags;
    u16 high_addr;
} __attribute__((packed));

struct idt_entry the_idt[256];

static struct
{
    u16 size;
    u32 ptr;
} __attribute__((packed)) idtptr = { 8 * 256 - 1, (u32)&the_idt };

static
void set_gate(u8 gate_num, u32 addr, u16 seg_sel, u8 flags)
{
    struct idt_entry *g = &the_idt[gate_num];

    g->low_addr = addr & 0xFFFF;
    g->high_addr = (addr >> 16) & 0xFFFF;
    g->segment_selector = seg_sel;
    g->im_zero = 0;
    g->flags = flags;
}

void ack_int(u8 num)
{
    if (31 < num && num < 31 + 7)
        outb(0x20, 0x20);
    else
        outb(0xA0, 0x20);
}

void isr_timer(void)
{
    //serial_printf("TIMER\n");
    ack_int(32);
}

void isr_kbd(void)
{
    char c = inb(0x60);
    serial_printf("kbd! %x\n", c);
    ack_int(33);
}

void load_idt(void)
{
    (void) idtptr;
    memset(the_idt, 0, sizeof (the_idt));

    set_gate(0, (u32)isr0, 8, 0x8E);
    set_gate(1, (u32)isr1, 8, 0x8E);
    set_gate(2, (u32)isr2, 8, 0x8E);
    set_gate(3, (u32)isr3, 8, 0x8E);
    set_gate(4, (u32)isr4, 8, 0x8E);
    set_gate(5, (u32)isr5, 8, 0x8E);
    set_gate(6, (u32)isr6, 8, 0x8E);
    set_gate(7, (u32)isr7, 8, 0x8E);
    set_gate(8, (u32)isr8, 8, 0x8E);
    set_gate(9, (u32)isr9, 8, 0x8E);
    set_gate(10, (u32)isr10, 8, 0x8E);
    set_gate(11, (u32)isr11, 8, 0x8E);
    set_gate(12, (u32)isr12, 8, 0x8E);
    set_gate(13, (u32)isr13, 8, 0x8E);
    set_gate(14, (u32)isr14, 8, 0x8E);
    set_gate(15, (u32)isr15, 8, 0x8E);
    set_gate(16, (u32)isr16, 8, 0x8E);
    set_gate(17, (u32)isr17, 8, 0x8E);
    set_gate(18, (u32)isr18, 8, 0x8E);
    set_gate(19, (u32)isr19, 8, 0x8E);
    set_gate(20, (u32)isr20, 8, 0x8E);
    set_gate(21, (u32)isr21, 8, 0x8E);
    set_gate(22, (u32)isr22, 8, 0x8E);
    set_gate(23, (u32)isr23, 8, 0x8E);
    set_gate(24, (u32)isr24, 8, 0x8E);
    set_gate(25, (u32)isr25, 8, 0x8E);
    set_gate(26, (u32)isr26, 8, 0x8E);
    set_gate(27, (u32)isr27, 8, 0x8E);
    set_gate(28, (u32)isr28, 8, 0x8E);
    set_gate(29, (u32)isr29, 8, 0x8E);
    set_gate(30, (u32)isr30, 8, 0x8E);
    set_gate(31, (u32)isr31, 8, 0x8E);

    set_gate(32, (u32)_isr_timer, 8, 0x8E);
    set_gate(33, (u32)_isr_kbd, 8, 0x8E);

    __asm__("cli\n"
            "lidt idtptr\n"
            "sti\n");
    serial_printf("idt ok\n");
}

void common_isr(u8 num)
{
    serial_printf("INT (%x)\n", num);
    ack_int(num);
}
