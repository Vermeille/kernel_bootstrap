.extern common_isr

.macro isr_noerror n
.globl isr\n
isr\n:

    pusha

    push $0
    push $\n

    call common_isr

    add $0x08, %esp

    popa

    iret
.endm

.macro isr_error n
.globl isr\n
isr\n:

    pusha

    push $\n

    call common_isr

    add $4, %esp

    popa

    iret
.endm

isr_noerror 0
isr_noerror 1
isr_noerror 2
isr_noerror 3
isr_noerror 4
isr_noerror 5
isr_noerror 6
isr_noerror 7
isr_error 8
isr_noerror 9
isr_error 10
isr_error 11
isr_error 12
isr_error 13
isr_error 14
isr_noerror 15
isr_noerror 16
isr_noerror 17
isr_noerror 18
isr_noerror 19
isr_noerror 20
isr_noerror 21
isr_noerror 22
isr_noerror 23
isr_noerror 24
isr_noerror 25
isr_noerror 26
isr_noerror 27
isr_noerror 28
isr_noerror 29
isr_noerror 30
isr_noerror 31

.globl _isr_timer
_isr_timer:
    pusha
    call isr_timer
    popa
    iret

.globl _isr_kbd
_isr_kbd:
    pusha
    call isr_kbd
    popa
    iret

