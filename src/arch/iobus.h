#pragma once
#include "types.h"

void outb(u16 addr, u8 val);
int inl(u16 addr);
void outl(u16 addr, u32 val);
u8 inb(u16 port);
