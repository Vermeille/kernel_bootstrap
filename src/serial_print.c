#include <arch/iobus.h>
#include "print_impl.h"

void serial_print_char(unsigned char c)
{
    while ((inb(0x3f8 + 5) & 0x20) == 0)
        ;
    outb(0x3F8, c);
}

io_print(serial_)
