#include <stdarg.h>

#define io_print(OUTPUT)                        \
static void OUTPUT##print_string(const char* str)    \
{                                               \
    for (int i = 0; str[i]; ++i)                \
        OUTPUT##print_char(str[i]);                  \
}                                               \
                                                \
static void OUTPUT##print_hex(unsigned int val)      \
{                                                               \
    const char hex_char[] =                                     \
        {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',      \
            'a', 'b', 'c', 'd', 'e', 'f' };                     \
                                                                \
    OUTPUT##print_string("0x");                                      \
                                                                \
    for (int i = sizeof (int) - 1; i >= 0; --i)                 \
    {                                                           \
        OUTPUT##print_char(hex_char[(val >> (i * 8 + 4)) & 0xf]);    \
        OUTPUT##print_char(hex_char[(val >> (i * 8)) & 0xf]);        \
    }                                                           \
}                                                               \
\
void OUTPUT##printf(const char* fmt, ...)\
{\
    char* str;\
    unsigned int d;\
    char c;\
    va_list args;\
    va_start(args, fmt);\
    while (*fmt)\
    {\
        if (*fmt == '%')\
        {\
            ++fmt;\
            switch (*fmt)\
            {\
                case '%':\
                    OUTPUT##print_char('%');\
                    break;\
                case 'x':\
                    d = va_arg(args, unsigned int);\
                    OUTPUT##print_hex(d);\
                    break;\
                case 's':\
                    str = va_arg(args, char*);\
                    OUTPUT##print_string(str);\
                    break;\
                case 'c':\
                    c = va_arg(args, int);\
                    OUTPUT##print_char(c);\
                    break;\
            }\
        }\
        else\
            OUTPUT##print_char(*fmt);\
        ++fmt;\
    }\
    va_end(args);\
}\
\

