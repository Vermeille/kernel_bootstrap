#include <utils.h>

void memset(void *addr, int val, unsigned int sz)
{
    char *a = (char*)addr;
    while (sz)
    {
        *a = val;
        ++a;
        --sz;
    }
}

void memcpy(char* dst, const char* src, int sz)
{
    while (sz)
    {
        *dst = *src;
        ++dst;
        ++src;
        --sz;
    }
}

