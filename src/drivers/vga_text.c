#include "vga_text.h"

static struct vga_color color;

u8 get_vga_color_code(void)
{
    return *(u8*)&color;
}

void set_vga_colors(struct vga_color cols)
{
    color = cols;
}

void set_vga_fg_col(enum vga_colors c)
{
    color.fg_col = c;
}

void set_vga_bg_col(enum vga_colors c)
{
    color.bg_col = c;
}
