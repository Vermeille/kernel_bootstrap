#include <arch/iobus.h>

u32 get_keyev_if_any(void)
{
    if ((inb(0x64) & 1) == 0)
        return -1;
    return inb(0x60);
}

// I seriously doubt needing this. It blocks the whole kernel!
u32 get_keyev(void)
{
    while ((inb(0x64) & 1) == 0)
        ;
    return inb(0x60);
}
