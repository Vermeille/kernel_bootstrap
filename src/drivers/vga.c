#include "vga.h"
#include <arch/iobus.h>

#define NB_ATTR_REGS 21
#define NB_SEQ_REGS 5
#define NB_GRAPH_REGS 9
#define NB_CRT_REGS 25

typedef struct
{
    char attr_regs[2 * NB_ATTR_REGS];
    char misc_output;
    char seq_regs[2 * NB_SEQ_REGS];
    char graph_regs[2 * NB_GRAPH_REGS];
    char crt_regs[2 * NB_CRT_REGS];
} regs_settings;

static regs_settings cols256_linear =
{
    {
        0x00, 0x00,
        0x01, 0x01,
        0x02, 0x02,
        0x03, 0x03,
        0x04, 0x04,
        0x05, 0x05,
        0x06, 0x06,
        0x07, 0x07,
        0x08, 0x08,
        0x09, 0x09,
        0x0A, 0x0A,
        0x0B, 0x0B,
        0x0C, 0x0C,
        0x0D, 0x0D,
        0x0E, 0x0E,
        0x0F, 0x0F,
        0x10, 0x41,
        0x11, 0x00,
        0x12, 0x0F,
        0x13, 0x00,
        0x14, 0x00
    },
    0x63,
    {
        0x00, 0x03,
        0x01, 0x01,
        0x02, 0x0F,
        0x03, 0x00,
        0x04, 0x0E,
    },
    {
        0x00, 0x00,
        0x01, 0x00,
        0x02, 0x00,
        0x03, 0x00,
        0x04, 0x00,
        0x05, 0x40,
        0x06, 0x05,
        0x07, 0x0F,
        0x08, 0xFF,
    },
    {
        0x00, 0x5F,
        0x01, 0x4F,
        0x02, 0x50,
        0x03, 0x82,
        0x04, 0x54,
        0x05, 0x80,
        0x06, 0xBF,
        0x07, 0x1F,
        0x08, 0x00,
        0x09, 0x41,
        0x0A, 0x00,
        0x0B, 0x00,
        0x0C, 0x00,
        0x0D, 0x00,
        0x0E, 0x00,
        0x0F, 0x00,
        0x10, 0x9C,
        0x11, 0x0E,
        0x12, 0x8f,
        0x13, 0x28,
        0x14, 0x40,
        0x15, 0x96,
        0x16, 0xB9,
        0x17, 0xA3,
        0x18, 0xFF
    }
};

static regs_settings text_config =
{
    {
        0x00, 0x00,
        0x01, 0x01,
        0x02, 0x02,
        0x03, 0x03,
        0x04, 0x04,
        0x05, 0x05,
        0x06, 0x06,
        0x07, 0x07,
        0x08, 0x38,
        0x09, 0x39,
        0x0A, 0x3A,
        0x0B, 0x3B,
        0x0C, 0x3C,
        0x0D, 0x3D,
        0x0E, 0x3E,
        0x0F, 0x3F,
        0x10, 0x0C,
        0x11, 0x00,
        0x12, 0x0F,
        0x13, 0x08,
        0x14, 0x00
    },
    0x67,
    {
        0x00, 0x03,
        0x01, 0x00,
        0x02, 0x03,
        0x03, 0x00,
        0x04, 0x02,
    },
    {
        0x00, 0x00,
        0x01, 0x00,
        0x02, 0x00,
        0x03, 0x00,
        0x04, 0x00,
        0x05, 0x10,
        0x06, 0x0E,
        0x07, 0x00,
        0x08, 0xFF,
    },
    {
        0x00, 0x5F,
        0x01, 0x4F,
        0x02, 0x50,
        0x03, 0x82,
        0x04, 0x55,
        0x05, 0x81,
        0x06, 0xBF,
        0x07, 0x1F,
        0x08, 0x00,
        0x09, 0x4F,
        0x0A, 0x00,
        0x0B, 0x00,
        0x0C, 0x00,
        0x0D, 0x00,
        0x0E, 0x00,
        0x0F, 0x00,
        0x10, 0x9C,
        0x11, 0x8E,
        0x12, 0x8f,
        0x13, 0x28,
        0x14, 0x1F,
        0x15, 0x96,
        0x16, 0xB9,
        0x17, 0xA3,
        0x18, 0xFF
    }
};

static unsigned int g_palette[256] =
{
    0x0, 0x800000, 0x8000, 0x808000,
    0x80, 0x800080, 0x8080, 0xc0c0c0,
    0xc0dcc0, 0xa6caf0, 0x402000, 0x602000,
    0x802000, 0xa02000, 0xc02000, 0xe02000,
    0x4000, 0x204000, 0x404000, 0x604000,
    0x804000, 0xa04000, 0xc04000, 0xe04000,
    0x6000, 0x206000, 0x406000, 0x606000,
    0x806000, 0xa06000, 0xc06000, 0xe06000,
    0x8000, 0x208000, 0x408000, 0x608000,
    0x808000, 0xa08000, 0xc08000, 0xe08000,
    0xa000, 0x20a000, 0x40a000, 0x60a000,
    0x80a000, 0xa0a000, 0xc0a000, 0xe0a000,
    0xc000, 0x20c000, 0x40c000, 0x60c000,
    0x80c000, 0xa0c000, 0xc0c000, 0xe0c000,
    0xe000, 0x20e000, 0x40e000, 0x60e000,
    0x80e000, 0xa0e000, 0xc0e000, 0xe0e000,
    0x40, 0x200040, 0x400040, 0x600040,
    0x800040, 0xa00040, 0xc00040, 0xe00040,
    0x2040, 0x202040, 0x402040, 0x602040,
    0x802040, 0xa02040, 0xc02040, 0xe02040,
    0x4040, 0x204040, 0x404040, 0x604040,
    0x804040, 0xa04040, 0xc04040, 0xe04040,
    0x6040, 0x206040, 0x406040, 0x606040,
    0x806040, 0xa06040, 0xc06040, 0xe06040,
    0x8040, 0x208040, 0x408040, 0x608040,
    0x808040, 0xa08040, 0xc08040, 0xe08040,
    0xa040, 0x20a040, 0x40a040, 0x60a040,
    0x80a040, 0xa0a040, 0xc0a040, 0xe0a040,
    0xc040, 0x20c040, 0x40c040, 0x60c040,
    0x80c040, 0xa0c040, 0xc0c040, 0xe0c040,
    0xe040, 0x20e040, 0x40e040, 0x60e040,
    0x80e040, 0xa0e040, 0xc0e040, 0xe0e040,
    0x80, 0x200080, 0x400080, 0x600080,
    0x800080, 0xa00080, 0xc00080, 0xe00080,
    0x2080, 0x202080, 0x402080, 0x602080,
    0x802080, 0xa02080, 0xc02080, 0xe02080,
    0x4080, 0x204080, 0x404080, 0x604080,
    0x804080, 0xa04080, 0xc04080, 0xe04080,
    0x6080, 0x206080, 0x406080, 0x606080,
    0x806080, 0xa06080, 0xc06080, 0xe06080,
    0x8080, 0x208080, 0x408080, 0x608080,
    0x808080, 0xa08080, 0xc08080, 0xe08080,
    0xa080, 0x20a080, 0x40a080, 0x60a080,
    0x80a080, 0xa0a080, 0xc0a080, 0xe0a080,
    0xc080, 0x20c080, 0x40c080, 0x60c080,
    0x80c080, 0xa0c080, 0xc0c080, 0xe0c080,
    0xe080, 0x20e080, 0x40e080, 0x60e080,
    0x80e080, 0xa0e080, 0xc0e080, 0xe0e080,
    0xc0, 0x2000c0, 0x4000c0, 0x6000c0,
    0x8000c0, 0xa000c0, 0xc000c0, 0xe000c0,
    0x20c0, 0x2020c0, 0x4020c0, 0x6020c0,
    0x8020c0, 0xa020c0, 0xc020c0, 0xe020c0,
    0x40c0, 0x2040c0, 0x4040c0, 0x6040c0,
    0x8040c0, 0xa040c0, 0xc040c0, 0xe040c0,
    0x60c0, 0x2060c0, 0x4060c0, 0x6060c0,
    0x8060c0, 0xa060c0, 0xc060c0, 0xe060c0,
    0x80c0, 0x2080c0, 0x4080c0, 0x6080c0,
    0x8080c0, 0xa080c0, 0xc080c0, 0xe080c0,
    0xa0c0, 0x20a0c0, 0x40a0c0, 0x60a0c0,
    0x80a0c0, 0xa0a0c0, 0xc0a0c0, 0xe0a0c0,
    0xc0c0, 0x20c0c0, 0x40c0c0, 0x60c0c0,
    0x80c0c0, 0xa0c0c0, 0xfffbf0, 0xa0a0a4,
    0x808080, 0xff0000, 0xff00, 0xffff00,
    0xff, 0xff00ff, 0xffff, 0xffffff
};

static void vga_load_settings(regs_settings* r)
{
    outb(0x3C2, r->misc_output);

    for (int i = 0; i < NB_SEQ_REGS; ++i)
    {
        outb(0x3C4, r->seq_regs[i * 2]);
        outb(0x3C4 + 1, r->seq_regs[i * 2 + 1]);
    }

    outb(0x3D4, 0x03);
    outb(0x3D4 + 1, inb(0x3D4 + 1) | 0x80);

#if 0
    outb(0x3D4, 0x11);
    outb(0x3D4 + 1, inb(0x3D4 + 1) & ~0x80);
#endif

    r->crt_regs[0x03] |= 0x80;
    r->crt_regs[0x11] &= ~0x80;

    for (int i = 0; i < NB_CRT_REGS; ++i)
    {
        outb(0x3D4, r->graph_regs[i * 2]);
        outb(0x3D4 + 1, r->graph_regs[i * 2 + 1]);
    }

    for (int i = 0; i < NB_GRAPH_REGS; ++i)
    {
        outb(0x3CE, r->graph_regs[i * 2]);
        outb(0x3CE + 1, r->graph_regs[i * 2 + 1]);
    }

    inb(0x3DA); // set the port 0x3C0 to wait an index
    for (int i = 0; i < NB_ATTR_REGS; ++i)
    {
        outb(0x3C0, r->attr_regs[i * 2]);
        outb(0x3C0, r->attr_regs[i * 2 + 1]);
    }
}

static void vga_load_palette(const unsigned int* palette)
{
    inb(0x3DA);
    outb(0x3C0, 0x20);
    outb(0x3C6, 0xFF);
    outb(0x3C8, 0);
    for (int i = 0; i < 256; ++i)
    {
        outb(0x3C9, (palette[i] >> 18) & 0xFF);
        outb(0x3C9, (palette[i] >> 10) & 0xFF);
        outb(0x3C9, (palette[i] >> 2) & 0xFF);
    }
}

void vga_set_px_mode()
{
    vga_load_settings(&cols256_linear);
    vga_load_palette(g_palette);
}

void vga_set_text_mode()
{
    vga_load_settings(&text_config);
}

inline
void vga_draw_px(u16 lin, u16 column, u32 color)
{
    *(char*)(VGA_ADDR + lin * VGA_SCREEN_WIDTH + column) = color;
}

void vga_draw_rect(u16 y, u16 x, u16 y2, u16 x2, u32 color)
{
    u8 *line_ptr = (u8*)(VGA_ADDR + y * VGA_SCREEN_WIDTH + x);

    for (u16 i = y; i <= y2; ++i)
    {
        u8 *ptr = line_ptr;
        for (u16 j = x; j <= x2; ++j)
        {
            *ptr = color;
            ++ptr;
        }
        line_ptr += VGA_SCREEN_WIDTH;
    }
}

void vga_draw_circle(u16 radius, u16 y_center, u16 x_center, u32 col)
{
    u16 x = 0;
    u16 y = radius;
    short m = 5 - 4 * radius;

    while (x <= y)
    {
        vga_draw_px(y + y_center, x + x_center, col);
        vga_draw_px(x + y_center, y + x_center, col);
        vga_draw_px(y + y_center, -x + x_center, col);
        vga_draw_px(x + y_center, -y + x_center, col);
        vga_draw_px(-y + y_center, x + x_center, col);
        vga_draw_px(-x + y_center, y + x_center, col);
        vga_draw_px(-y + y_center, -x + x_center, col);
        vga_draw_px(-x + y_center, -y + x_center, col);

        if (m > 0)
        {
            --y;
            m -= 8 * y;
        }
        ++x;
        m += 8 * x + 4;
    }
}
