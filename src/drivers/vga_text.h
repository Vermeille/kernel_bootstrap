#pragma once
#include <types.h>

// TODO: Pleaaaase, make me reentrant!

u8 get_vga_color_code(void);

enum vga_colors
{
    BLACK = 0,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    BROWN,
    LIGHT_GRAY,
    DARK_GRAY,
    LIGHT_BLUE,
    LIGHT_GREEN,
    LIGHT_CYAN,
    LIGHT_RED,
    LIGHT_MAGENTA,
    LIGHT_BROWN,
    WHITE,
};

struct vga_color
{
    u8 fg_col:3;
    u8 fg_intense:1;
    u8 bg_col:3;
    u8 bg_intense:1;
} __attribute__((packed));

void set_vga_colors(struct vga_color cols);
void set_vga_fg_col(enum vga_colors c);
void set_vga_bg_col(enum vga_colors c);

