#pragma once

#include <types.h>

#define VGA_SCREEN_WIDTH 320
#define VGA_SCREEN_HEIGHT 200
#define VGA_ADDR 0xA0000

void vga_set_text_mode();
void vga_set_px_mode();
void vga_draw_px(u16 lin, u16 column, u32 color);
void vga_draw_rect(u16 y, u16 x, u16 y2, u16 x2, u32 color);
void vga_draw_circle(u16 radius, u16 y_center, u16 x_center, u32 col);
