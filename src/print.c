#include "print_impl.h"
#include "print.h"
#include <utils.h>
#include <types.h>
#include <drivers/vga_text.h>

#define VGA_TEXT 0xB8000
#define CHARS_PER_LINE 80
#define LINES 25
#define LINE_SIZE (2 * CHARS_PER_LINE)

static struct
{
    int line;
    int col;
} g_pos = {0, 0};

struct vga_char
{
    char c;
    u8 col;
} __attribute__((packed));

void clr_scr()
{
    memset((char*)VGA_TEXT, 0, LINE_SIZE * LINES);
}

void print_char(unsigned char c)
{
    static struct vga_char *buffer = (struct vga_char*)VGA_TEXT;

    if (c == '\n' || g_pos.col == CHARS_PER_LINE)
    {
        ++g_pos.line;
        g_pos.col = 0;
        if (c == '\n')
            return;
    }
    if (g_pos.line == 25)
    {
        memcpy((char*)VGA_TEXT, (char*)VGA_TEXT + LINE_SIZE, 24*80*2);
        g_pos.line = 24;
        g_pos.col = 0;
    }
    u8 cols = get_vga_color_code();
    buffer[CHARS_PER_LINE * g_pos.line + g_pos.col] = (struct vga_char){ c, cols };
    ++g_pos.col;
}

io_print()
