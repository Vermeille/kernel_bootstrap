#include "multiboot.h"
#include "print.h"
#include "arch/gdt.h"
#include "arch/idt.h"
#include "arch/pic.h"
#include <drivers/vga_text.h>
#include <drivers/vga.h>

void kernel(unsigned long magic, multiboot_info_t* info)
{
    (void) magic;
    (void) info;

    load_gdt();

    init_pic();
    load_idt();

    clr_scr();

    struct vga_color col = {
        .fg_col = GREEN,
        .fg_intense = 1,
        .bg_col = LIGHT_GREEN,
    };

    set_vga_colors(col);
    printf("Welcome to PanglOS!\n");
    printf("Despite being so great, you can't do anything know. Take a break!\n");
    serial_printf("Welcome!\n");

    /*
    vga_set_px_mode();

    for (int i = 0; i < VGA_SCREEN_HEIGHT; ++i)
    {
        for (int j = 0; j < VGA_SCREEN_WIDTH; ++j)
            vga_draw_px(i, j, i % 256);
    }

    vga_draw_circle(30, 30, 60, 4);
    //*/

    while (1)
        __asm__ ("hlt");

    return;
}
