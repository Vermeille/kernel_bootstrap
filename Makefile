all: build

boot: build
	qemu -kernel src/kern -serial stdio -nographic

build:
	make -C src all

clean:
	make -C src clean
